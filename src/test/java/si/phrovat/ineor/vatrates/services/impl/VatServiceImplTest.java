package si.phrovat.ineor.vatrates.services.impl;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import si.phrovat.ineor.vatrates.controllers.Order;
import si.phrovat.ineor.vatrates.dto.Country;
import si.phrovat.ineor.vatrates.dto.Period;
import si.phrovat.ineor.vatrates.dto.VatCountries;
import si.phrovat.ineor.vatrates.services.VatService;
import si.phrovat.ineor.vatrates.services.clients.VatClient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class VatServiceImplTest {

    private static final Integer DEFAULT_SIZE = 3;

    VatService vatService;

    @MockBean
    VatClient vatClient;

    @BeforeEach
    void before() {
        MockitoAnnotations.initMocks(this);
        vatService = new VatServiceImpl(vatClient);
    }

    @Test
    void listVatCountriesDefault() {
        given(vatClient.getVatCountries()).willReturn(getDefaultListOfVatCountries());
        VatCountries vatCountries = vatService.listVatCountries(Order.ASC, DEFAULT_SIZE);

        assertThat("Element is not null", vatCountries, is(notNullValue()));
        assertThat("List is not empty", vatCountries.getRates(), is(Matchers.not(empty())));
        assertThat("List is of size 3", vatCountries.getRates().size(), is(equalTo(3)));
        assertThat("First element standard rate is 20", vatCountries.getRates().get(0).getPeriods().get(0).getRates().get("standard"), is(equalTo(20f)));
    }

    @Test
    void listVatCountriesWithSize() {
        given(vatClient.getVatCountries()).willReturn(getDefaultListOfVatCountries());
        int size = 2;
        VatCountries vatCountries = vatService.listVatCountries(Order.ASC, size);

        assertThat("Element is not null", vatCountries, is(notNullValue()));
        assertThat("List is not empty", vatCountries.getRates(), is(Matchers.not(empty())));
        assertThat(String.format("List is of size %d", size), vatCountries.getRates().size(), is(equalTo(size)));
        assertThat("First element standard rate is 20", vatCountries.getRates().get(0).getPeriods().get(0).getRates().get("standard"), is(equalTo(20f)));
    }

    @Test
    void listVatRatesWithOrderAndSize() {
        given(vatClient.getVatCountries()).willReturn(getDefaultListOfVatCountries());
        int size = 2;
        VatCountries vatCountries = vatService.listVatCountries(Order.DESC, size);

        assertThat("Element is not null", vatCountries, is(notNullValue()));
        assertThat("List is not empty", vatCountries.getRates(), is(Matchers.not(empty())));
        assertThat(String.format("List is of size %d", size), vatCountries.getRates().size(), is(equalTo(size)));
        assertThat("First element standard rate is 25", vatCountries.getRates().get(0).getPeriods().get(0).getRates().get("standard"), is(equalTo(25f)));
    }

    @Test
    void listVatRatesWithInvalidSize() {
        VatCountries defaultListOfVatCountries = getDefaultListOfVatCountries();
        int size = defaultListOfVatCountries.getRates().size();
        given(vatClient.getVatCountries()).willReturn(defaultListOfVatCountries);
        VatCountries vatCountries = vatService.listVatCountries(Order.DESC, size + 1);

        assertThat("Element is not null", vatCountries, is(notNullValue()));
        assertThat("List is not empty", vatCountries.getRates(), is(Matchers.not(empty())));
        assertThat(String.format("List is of size %d", size), vatCountries.getRates().size(), is(equalTo(size)));
        assertThat("First element standard rate is 25", vatCountries.getRates().get(0).getPeriods().get(0).getRates().get("standard"), is(equalTo(25f)));
    }

    private VatCountries getDefaultListOfVatCountries() {
        List<Country> countries = new ArrayList<>();
        List<Period> sloPeriod = new ArrayList<>();
        sloPeriod.add(new Period(LocalDate.of(2019, 6, 1),
                Map.of("standard", 22f)));
        countries.add(new Country("Slovenia",
                "SLO",
                "SI",
                sloPeriod));

        List<Period> hrPeriod = new ArrayList<>();
        hrPeriod.add(new Period(LocalDate.of(2019, 6, 1),
                Map.of("standard", 25f)));
        countries.add(new Country("Croatia",
                "HR",
                "HR",
                hrPeriod));

        List<Period> bePeriod = new ArrayList<>();
        bePeriod.add(new Period(LocalDate.of(2019, 1, 1),
                Map.of("standard", 21f, "reduced", 12f)));
        bePeriod.add(new Period(LocalDate.of(2018, 1, 1),
                Map.of("standard", 20f)));
        countries.add(new Country("Belgium",
                "BE",
                "BE",
                bePeriod));

        List<Period> frPeriod = new ArrayList<>();
        frPeriod.add(new Period(LocalDate.of(2018, 1, 1),
                Map.of("standard", 20f)));
        countries.add(new Country("France",
                "FR",
                "FR",
                frPeriod));

        return new VatCountries(countries);
    }
}