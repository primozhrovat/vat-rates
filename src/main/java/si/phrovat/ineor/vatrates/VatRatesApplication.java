package si.phrovat.ineor.vatrates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class VatRatesApplication {

	public static void main(String[] args) {
		SpringApplication.run(VatRatesApplication.class, args);
	}

}
