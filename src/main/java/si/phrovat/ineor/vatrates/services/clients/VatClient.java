package si.phrovat.ineor.vatrates.services.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import si.phrovat.ineor.vatrates.dto.VatCountries;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@FeignClient(name = "vat-client", url = "${vat-client.url}")
public interface VatClient {

    @RequestMapping(method = RequestMethod.GET)
    VatCountries getVatCountries();
}
