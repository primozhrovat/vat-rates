package si.phrovat.ineor.vatrates.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.phrovat.ineor.vatrates.controllers.Order;
import si.phrovat.ineor.vatrates.dto.Country;
import si.phrovat.ineor.vatrates.dto.Period;
import si.phrovat.ineor.vatrates.dto.VatCountries;
import si.phrovat.ineor.vatrates.services.VatService;
import si.phrovat.ineor.vatrates.services.clients.VatClient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@Service
public class VatServiceImpl implements VatService {

    private static final String STANDARD = "standard";
    private VatClient vatClient;

    private Cache cache;

    @Autowired
    protected VatServiceImpl(VatClient vatClient) {
        this.vatClient = vatClient;
        this.cache = null;
    }

    @Override
    public VatCountries listVatCountries(Order order, Integer size) {

        if (cache == null || cache.localDate.isBefore(LocalDate.now())) processCountries();


        int resultSize = size > cache.countries.size() ? cache.countries.size() : size;
        int start = 0, step = 1;

        if (order == Order.DESC) {
            start = cache.countries.size() - 1;
            step = -1;
        }
        List<Country> resultList = new ArrayList<>();
        for (int i = start; shouldEnd(order, cache.countries.size(), resultSize, i); i += step) {
            resultList.add(cache.countries.get(i));
        }

        return new VatCountries(resultList);
    }

    private boolean shouldEnd(Order order, int listSize, int resultSize, int i) {
        if (Order.DESC == order) {
            return i >= listSize - resultSize;
        } else {
            return i < resultSize;
        }
    }

    private void processCountries() {
        VatCountries vatCountries = vatClient.getVatCountries();
        this.cache = new Cache();
        this.cache.localDate = LocalDate.now();
        this.cache.countries = vatCountries.getRates().stream()
                    .map(country -> {
                        country.getPeriods().sort(Comparator.comparing(Period::getEffectiveFrom).reversed());
                        Period latestPeriod = country.getPeriods().get(0);

                        return new Country(country.getName(), country.getCode(), country.getCountryCode(), Collections.singletonList(latestPeriod));
                    })
                    .sorted(Comparator.comparing(o -> o.getPeriods().get(0).getRates().get(STANDARD)))
                    .collect(Collectors.toList());
    }

    private static class Cache {
        private LocalDate localDate;
        private List<Country> countries;
    }
}
