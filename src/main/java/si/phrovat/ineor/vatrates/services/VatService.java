package si.phrovat.ineor.vatrates.services;

import si.phrovat.ineor.vatrates.controllers.Order;
import si.phrovat.ineor.vatrates.dto.VatCountries;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */
public interface VatService {

    VatCountries listVatCountries(Order order, Integer size);
}
