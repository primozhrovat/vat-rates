package si.phrovat.ineor.vatrates.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Period {

    @JsonDeserialize(using = CustomLocalDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("effective_from")
    private LocalDate effectiveFrom;

    private Map<String, Float> rates;
}
