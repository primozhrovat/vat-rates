package si.phrovat.ineor.vatrates.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Country {

    private String name;

    private String code;

    @JsonProperty("country_code")
    private String countryCode;

    private List<Period> periods;
}
