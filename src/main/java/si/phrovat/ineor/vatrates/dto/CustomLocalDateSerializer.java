package si.phrovat.ineor.vatrates.dto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 *
 * This class is used to prevent exception when parsing invalid year provided in API e.g. 0000-01-01 is invalid in LocalDate
 */

@Slf4j
public class CustomLocalDateSerializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String text = jsonParser.getText();

        try {
            LocalDate date = LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            return date;
        } catch (DateTimeParseException e) {
            log.warn("Error parsing date {}", e.getParsedString());
            return LocalDate.of(1, 1, 1);
        }
    }
}
