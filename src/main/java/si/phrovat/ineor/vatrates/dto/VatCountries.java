package si.phrovat.ineor.vatrates.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@Data
@NoArgsConstructor
public class VatCountries {
    private String details;
    private List<Country> rates;

    public VatCountries(List<Country> rates) {
        this.rates = rates;
    }
}
