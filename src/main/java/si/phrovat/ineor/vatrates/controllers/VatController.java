package si.phrovat.ineor.vatrates.controllers;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import si.phrovat.ineor.vatrates.dto.VatCountries;
import si.phrovat.ineor.vatrates.services.VatService;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@RestController
@RequestMapping("v1")
public class VatController {

    private VatService vatService;

    @Autowired
    protected VatController(VatService vatService) {
        this.vatService = vatService;
    }

    @GetMapping("vat-rates")
    @ApiOperation(value = "Get VAT rates by country")
    public ResponseEntity<VatCountries> getVatCountries(@RequestParam(value = "size", required = false, defaultValue = "3") Integer size,
                                                        @RequestParam(value = "order", required = false, defaultValue = "ASC") Order order) {
        VatCountries vatCountries = vatService.listVatCountries(order, size);

        return ResponseEntity.ok(vatCountries);
    }
}
