package si.phrovat.ineor.vatrates.controllers;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */
public enum Order {
    ASC, DESC
}
