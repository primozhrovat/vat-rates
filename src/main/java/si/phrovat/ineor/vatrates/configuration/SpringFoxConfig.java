package si.phrovat.ineor.vatrates.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Primož Hrovat
 * @since 1.0.0
 */

@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("si.phrovat.ineor.vatrates"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title("VAT Rates API")
                        .description("This API returns a list of EU countries, ordered by their standard VAT rates.")
                        .version("1.0.0")
                        .contact(new Contact("Primož Hrovat", "", "primoz.hrovat.96@gmail.com"))
                        .license("WTFPL")
                        .licenseUrl("http://www.wtfpl.net/")
                        .build());
    }
}
